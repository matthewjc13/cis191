#!/bin/bash

echo Searching for $1 >> log/history_of_searches.txt

search_url="http://www.imdb.com/find?q=${1}"

echo $search_url >> log/history_of_searches.txt

wget -q -O search_html $search_url

cat search_html |
while read line
do
  if [[ $line == *"result_text"* ]]
  then
    echo $line | tr ' ' '\n' |
    while read line2
    do
      if [[ $line2 == *"href"* ]]
      then
        echo http://www.imdb.com$(echo $line2 | cut -d'"' -f 2)
        break
      fi
    done
    break
  fi
done
