# README #

To run this application, clone the repo onto a linux machine with internet access. Make a directory called 'log' and in that directory make a file called 'history_of_searches.txt'. Run 'npm install', then edit app.js to host the web server either locally or publicly, then run 'node app'. By visiting the ip that was set to host, you can enter movie titles to cause the linux machine to play movie descriptions through its speakers.

### Breakdown of Project ###

##### Shell Scripts #####

* get_movie_url.sh: Given a movie title, wget an imdb search page with the movie title as the query, then pattern match that page to find the url to the movie's imdb page. Example: ./get_movie_url.sh "The Avengers"
* get_description.sh: Given the url to a movie's imdb page, wget that page and pattern match the result to find the movie's description in the HTML. Example: ./get_description.sh "http://www.imdb.com/title/tt0848228/"
* get_summary_from_title.sh: Composition of the previous two scripts. Given a movie title, get the description of that movie by calling the second script on the output of the first script. Example: ./get_summary_from_title.sh "The Avengers"
* app.sh: Composition of the third script with text-to-speech. Given a movie title, get the description as text using the third script; then use the 'pico2wave' command to play that text out loud through the devices audio output device.

##### Web App #####

* package.json: file describing the project, as well as any dependencies.
* app.js: The file to run. The file sets up route bindings and starts up the server.
* views/main.ejs: The main view of the web app. It is a special renderable type of HTML that can take in different parameters. This html page has a form with an input field for a movie title and a submit button that executes the 'app.sh' shell script on that movie title. It also displays results or errors if they occur.
* routes/routes.js: The route handlers for the web app. Getting the main page '/' just renders main.ejs. Posting to the search route '/getmovie' actually executes the shell script on an inputted movie title.

##### Extra files #####

* .gitignore: set up to ignore my log folder plus some temporary files created by the scripts plus some extra node files.
* proposal.txt: my original project proposal.
* references.txt: a list of websites that I used a reference when developing this project.