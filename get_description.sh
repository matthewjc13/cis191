#!/bin/bash

echo Getting html for $1 >> log/history_of_searches.txt

wget -q -O movie_html $1

start=false

cat movie_html |
while read line
do
  if $start
  then
    if [[ $line == *"</div>"* ]]
    then
      break
    else
      echo $line
    fi
  else
    if [[ $line == *"summary_text"* ]]
    then
      start=true
    fi
  fi
done
