var express = require('express');
var routes  = require('./routes/routes.js');

var app = express();

app.use(express.bodyParser());
app.use(express.cookieParser());
app.use(express.logger("default"));
app.use(express.session({secret: "aSecret"}));

// Route handling
app.get('/', routes.get_main);
app.post('/getmovie', routes.get_movie);

// Run the server
app.listen(8080, "192.168.137.187");
console.log('Server running on port 8080');
