#!/bin/bash

text=$(./get_summary_from_title.sh "$1")

pico2wave -w summary.wav "$text" && aplay summary.wav

rm -f summary.wav

echo $text
