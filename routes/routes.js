var childProcess = require('child_process');

var getMain = function(req, res) {
  var movie   = !!req.session.lastMovie ? req.session.lastMovie : null;
  var summary = !!req.session.lastMovieSummary ?
                  req.session.lastMovieSummary : null;
  var error   = !!req.session.error ? req.session.error : null;

  res.render('main.ejs', {movie:movie, summary:summary, error:error});
}

var getMovie = function(req, res) {
  var movieTitle = req.body.movie_title;

  console.log(movieTitle);

  req.session.lastMovie = capitalize(movieTitle);

  // exec shell script
  childProcess.exec('./app.sh "' + movieTitle + '"',
    function(err, stdout, stderr) {
      if (stdout != null) {
        console.log("stdout: " + stdout);
        req.session.lastMovieSummary = stdout;
        req.session.error = null;
        res.redirect('/');
      }
      else if (stderr != null) {
        console.log("stderr: " + stderr);
        req.session.error = "Error getting movie info: " + stderr;
      }
      else if (err != null) {
        console.log("error: " + error);
        req.session.error = "Something went wrong: " + err;
      }
  });

  //res.redirect('/');
}

var capitalize = function(s) {
  return s.toLowerCase().replace( /\b./g, function(w) {
    return w.toUpperCase();
  });
}

var routes = {
  get_main: getMain,
  get_movie: getMovie
}

module.exports = routes;
